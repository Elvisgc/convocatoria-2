/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pojo;



public class Esudiantes {
    private int Calificacion;
    private String NombreAsignatura;
    private String Nombres;
    private String Cedula;
    private String Apellidos;
    private int Telefono;
    private String Carnet;
    
    public Esudiantes (){       
    }
    
    public Esudiantes(int Calificacion, String NombreAsignatura, String Nombres, String Cedula, String Apellidos, int Telefono, String Carnet){
        this.Apellidos = Apellidos;
        this.Calificacion = Calificacion;
        this.Carnet = Carnet;
        this.Cedula = Cedula;
        this.NombreAsignatura = NombreAsignatura;
        this.Nombres = Nombres;
        this.Telefono = Telefono;
        
    }
    
    public int getCalificacion() {
        return  Calificacion;
    }
    
    public void setCalificacion(int Calificacion) {
        this.Calificacion = Calificacion;
    }
    
    public String getApeliidos() {
        return Apellidos;
    }
    public void setApellidos(String Apellidos) {
        this.Apellidos = Apellidos;
       
    }
    
    public String getCedula() {
        return Cedula;      
    }
     public void setCedula(String Cedula) {
         this.Cedula = Cedula;
     }
     
     public String getNombres() {
         return Nombres;
     }
     
     public void setNombres(String Nombres) {
         this.Nombres = Nombres;
     }
     
     public String getCarnet() {
         return Carnet;            
     }
     
     public void setCarnet(String Carnet) {
         this.Carnet = Carnet;
     }
     
     public String getNombreAsignatura() {
         return NombreAsignatura;
     }
     
     public void setNombreAsignatura(String NombreAsignatura) {
         this.NombreAsignatura = NombreAsignatura;
     }
     
     public int getTelefono() {
         return Telefono;
     }
     
     public void setTelefono(int Telefono) {
         this.Telefono = Telefono;
     }
}

